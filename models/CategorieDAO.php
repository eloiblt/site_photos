<?php

require_once(PATH_ENTITY."Categorie.php");
require_once(PATH_MODELS.'DAO.php');

class CategorieDAO extends DAO
{
    function getAllCategorie()
    {
        $resultat = parent :: select('Select * from Categorie');
        $i = 0;
        foreach ($resultat as $donnee)
        {
            $tab[$i] = new Categorie($donnee['catId'] ,$donnee['nomCat']);
            $i++;
        }

        return $tab;
    }

    function getCategorieById($id)
    {
        $resultat = parent :: select('Select nomCat from Categorie where catId = ?', array($id));
        return $resultat[0][0];
    }

    function insertCat($nom)
    {
        return parent :: insert('insert into categorie(nomCat) values (?)', array($nom));
    }
} 

?>