<?php

require_once(PATH_MODELS."DAO.php");
require_once(PATH_ENTITY."Photo.php");

class PhotoDAO extends DAO
{
    public function getAllPhoto()
    {
        $resultat = parent :: select('Select * from Photo');
        $i = 0;
        if (count($resultat) >= 1)
        {
            foreach ($resultat as $donnee)
                {
                    $tab[$i] = new Photo($donnee['photoId'],$donnee['nomFich'],$donnee['description'],$donnee['catId']);
                    $i++;
                }
            return $tab;
        }
        return false;
    }

    public function getUnePhoto($id)
    {
        $resultat = parent :: select('Select * from Photo where photoId = ?', array($id));
        if (count($resultat) == 1)
            return new Photo($resultat[0]['photoId'],$resultat[0]['nomFich'],$resultat[0]['description'],$resultat[0]['catId']);
        return false;
    }

    public function getPhotosByCategories($categorie)
    {
        $resultat = parent :: select('select * from Photo where catId = ?', array($categorie));
        if ($resultat)
        {
            foreach ($resultat as $donnee)
            {
                $tab[] = new Photo($donnee['photoId'],$donnee['nomFich'],$donnee['description'],$donnee['catId']);
            }
            return $tab;
        }
        return false;
    }

    public function insertPhoto($nom, $description, $cat)
    {
        return parent :: insert('insert into photo(nomFich, description, catId) values (?,?,?)', array($nom, $description, $cat));
    }

    public function supprPhoto($id)
    {
        return parent :: insert('delete from photo where photoid = ?', array($id));
    }

}

?>