<?php

require_once(PATH_MODELS.'DAO.php');
require_once(PATH_ENTITY.'User.php');

class UserDAO extends DAO
{
    public function getUser($login)
    {
        $resultat = parent :: select('Select * from User where nom=?', array($login));

        if (count($resultat) == 1)
        {
            return new User($resultat[0]['id'], $resultat[0]['nom'], $resultat[0]['password']);
        }
        return false;
    }

    public function insertUser($name, $password)
    {
        return parent :: insert('insert into user(nom,password) values(?,?)', array($name, $password));
    }
} 

?>