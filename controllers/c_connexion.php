<?php

require_once(PATH_MODELS.'UserDAO.php');
require_once(PATH_ENTITY.'User.php');

if (isset($_POST['login']) && isset($_POST['pwd']))
{
    $login = htmlspecialchars($_POST['login']);
    $mdp = htmlspecialchars($_POST['pwd']);

    $userdao = new UserDAO(null);
    $user = $userdao->getUser($_POST['login']);

    if ($user)
    {
        if (password_verify($mdp, $user->getPassword()))
        {
            $_SESSION['login'] = $user->getNom();
            header('Location:index.php?page=accueil');
        }
    }
    else 
        $alert = choixAlert('User_Inconnu');
}   

require_once(PATH_VIEWS.$page.'.php');

