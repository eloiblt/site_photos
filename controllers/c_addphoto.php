<?php

require_once(PATH_MODELS.'CategorieDAO.php');
require_once(PATH_MODELS.'PhotoDAO.php');

$photodao = new PhotoDAO(null);
$categoriedao = new CategorieDAO(null);
$tab = $categoriedao->getAllCategorie();

if (isset($_POST['submit']))
{
    move_uploaded_file($_FILES['file']['tmp_name'], PATH_IMAGES . $_FILES['file']['name']);
    $photodao->insertPhoto($_FILES['file']['name'], $_POST['des'], $_POST['cat']);
    header('Location:index.php?page=accueil');
}

require_once(PATH_VIEWS.$page.'.php');      //appel de la vue 

?>


