<?php

require_once(PATH_MODELS.'CategorieDAO.php');

$categoriedao = new CategorieDAO(null);
$tab = $categoriedao->getAllCategorie();

if (isset($_POST['submit']))
{
    $ok = true;

    foreach($tab as $a)
    {
        if ($a->getNomCat() == $_POST['nom'])
        {
            $ok = false;
        }
    }

    if($ok)
    {
        $categoriedao->insertCat($_POST['nom']);
        header('Location:index.php?page=accueil');
    }
    else 
    {
        $alert = choixAlert('Cat_existante');
    }
}

require_once(PATH_VIEWS.$page.'.php');      //appel de la vue 

?>


