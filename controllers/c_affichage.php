<?php

require_once(PATH_MODELS.'PhotoDAO.php');
require_once(PATH_MODELS.'CategorieDAO.php');

$photoDao = new PhotoDAO(null);                 // creation des objets d'accès aux données
$categorieDao = new CategorieDAO(null);

if (isset($_GET['photo']))      //verification que nous avons bien recu en parametre un numero de photo à afficher 
{
    $numPhoto = (Int) $_GET['photo'];
    $photo = $photoDao->getUnePhoto($numPhoto);         //creation d'un objet photo correspondant à l'id passé avec GET
    if ($photo == null)
        $alert = choixAlert('Photo_Inexistante');
    require_once(PATH_VIEWS.$page.'.php'); 
}
else 
    header('Location:index.php');       //si pas de num photo en parametre de GET, affichage de l'index 

?>


