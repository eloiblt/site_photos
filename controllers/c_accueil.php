<?php

require_once(PATH_MODELS.'PhotoDAO.php');
require_once(PATH_MODELS.'CategorieDAO.php');

$photoDao = new PhotoDAO(null);             // creations des objets d'accès aux données
$categorieDAO = new CategorieDAO(null);

$tabAllCat = $categorieDAO->getAllCategorie();              //recuperation de toutes les categories 

if (isset($_GET['monselect']) && $_GET['monselect'] != "tout")      //si une valeur de la liste deroulante est selectionnée et qu'elle est différente de toutes les photos 
{                                                                   //cela signifie qu'on doit afficher les photos par categorie 
    $numberCategorie = (Int) htmlspecialchars($_GET['monselect']);    
    $tabPhotos = $photoDao->getPhotosByCategories($numberCategorie);        //recuperation des photos par catégorie
    if (!$tabPhotos)
    {
        $alert = choixAlert('Pas_Photo');
    }
}
else 
{
    $tabPhotos = $photoDao->getAllPhoto();          //si la liste déroulante n'a pas de valeur selectionnée, on recupère toutes les photos
}   

require_once(PATH_VIEWS.$page.'.php');      //appel de la vue 

?>


