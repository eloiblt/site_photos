<?php

require_once(PATH_MODELS.'UserDAO.php');

if (isset($_POST['login']) && isset($_POST['pwd']))
{
    if (trim($_POST['login']) != '' && trim($_POST['pwd']) != '')
    {
        $name = htmlspecialchars($_POST['login']);
        $password = htmlspecialchars($_POST['pwd']);
        $pass_hache = password_hash($password, PASSWORD_DEFAULT);

        $userdao = new UserDAO(null);
        $user = $userdao->getUser($name);

        if (!$user)
        {
            $rt = $userdao->insertUser($name, $pass_hache);
            $_SESSION['login'] = $name;
            header('Location:index.php?page=accueil');
        }
        else 
            $alert = choixAlert('Pseudo_pris');
    }
    else 
    {
        $alert = choixAlert('Infos_vides');
    }
}

require_once(PATH_VIEWS.$page.'.php');      //appel de la vue 

?>


