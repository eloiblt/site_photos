<?php

class Categorie
{
    private $catId;
    private $nomCat;

    public function __construct($catId, $nomCat)
    {
        $this->catId = $catId;
        $this->nomCat = $nomCat;
    }

    public function setCatId($catId)
    {
        if ($catId != " ")
            $this->catId = $catId;
    }

    public function getCatId() 
    {
        return $this->catId;
    }

    public function setNomCat($nomCat)
    {
        if ($nomCat != " ")
            $this->nomCat = $nomCat;
    }

    public function getNomCat() 
    {
        return $this->nomCat;
    }
}
?>