<?php

class Photo
{
    private $NomFich;
    private $nomFich;
    private $description;
    private $catID;

    public function __construct($photoId, $nomFich, $description, $catID)
    {
        $this->photoId = $photoId;
        $this->nomFich = $nomFich;
        $this->description = $description;
        $this->catID = $catID;
    }

    public function getPhotoId() 
    {
        return $this->photoId;
    }

    public function setPhotoId($photoId)
    {
        if ($photoId != " ")
            $this->photoId = $photoId;
    }

    public function getNomFich() 
    {
        return $this->nomFich;
    }
    
    public function setNomFich($nomFich)
    {
        if ($nomFich != " ")
            $this->nomFich = $nomFich;
    }

    public function getDescription() 
    {
        return $this->description;
    }
    
    public function setDescription($description)
    {
        if ($description != " ")
            $this->description = $description;
    }

    public function setCatID($catID)
    {
        if ($catID != " ")
            $this->catID = $catID;
    }

    public function getCatID() 
    {
        return $this->catID;
    }
}
?>