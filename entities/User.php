<?php

class User
{
    private $id;
    private $nom;
    private $password;

    public function __construct($a, $b, $c)
    {
        $this->id = $a;
        $this->nom = $b;
        $this->password = $c;
    }

    public function getId() 
    {
        return $this->nom;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getNom() 
    {
        return $this->nom;
    }

    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    public function getPassword() 
    {
        return $this->password;
    }
    
    public function setPassword($password)
    {
        $this->password = $password;
    }
}
?>