<?php

define('LOGO', 'Logo de la compagnie'); 
define('MENU_ACCUEIL','Accueil');
define('TEXTE_PAGE_404','Oops, la page demandée n\'existe pas !');
define('MESSAGE_ERREUR',"Une erreur s'est produite");
define('TITRE', 'Photo IUT');
define('ERREUR_QUERY', 'Problème d\'accès à la base de données. Contactez l\'administrateur');
define('MESSAGE_SAISIE','Quelles photos souhaitez-vous afficher ?');
define('TITRE_AFFICHAGE','Les détails sur cette photo');
define('ALL_PHOTOS','Toutes les photos');
define('ERREUR_PHOTO', 'Il n\'y a pas de photo dans cette categorie');
define('ERREUR_PHOTO_INEXISTANTE','Photo Inexistante');
define('DESCRIPTION', 'Description :');
define('NOMFICH','Nom du fichier :');
define('CATEGORIE','Categorie :');
define('PHOTOSELECT','photo(s) selectionnée(s)');
define('VALIDER','Valider');
define('INFODEST', 'Afficher des informations sur cette photo');
define('ERR_CO', 'Mauvais login ou mot de passe');
define('PSEUDO_PRIS', 'Ce pseudo est déja pris');
define('INFO_NULLES', 'Veuillez saisir des informations non nulles'); 
define('CAT_EXISTANTE', 'Cette catégorie est déja existante'); 

?>

