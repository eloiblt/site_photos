<?php require_once(PATH_VIEWS.'header.php');?>      

<?php require_once(PATH_VIEWS.'alert.php');?>       

<p>Saisissez les informations pour l'ajout d'une photo  :</p>

<form action="index.php?page=addphoto" method="post" enctype="multipart/form-data">
    <label for="a">Fichier : </label>
        <input type="file" id="a" name="file"><br>
    <label for="des">Description : </label><br>
        <input type="text" id="des" name="des"><br><br>
    <label for="cat">Categorie : </label><br>
        <select id ="cat" name="cat">
            <?php 
                foreach($tab as $element)
                {
                    echo '<option value="' . $element->getCatId() . '">' . $element->getNomCat() . '</option>';
                }
            ?>
        </select><br><br>
    <input type="submit" value="Envoyer" name="submit">
</form>

<?php require_once(PATH_VIEWS.'footer.php');