<?php
/*
 * DS PHP
 * Vue page index - page d'accueil
 *
 * Copyright 2016, Eric Dufour
 * http://techfacile.fr
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 *
 */
//  En tête de page
?>
<?php require_once(PATH_VIEWS.'header.php');?>      <!-- ajout du header-->

<!--  Zone message d'alerte -->
<?php require_once(PATH_VIEWS.'alert.php');?>       

<div class = "col-md-12 col-sm-12 col-xs-12">
    <div class="alert alert-success">           <!-- affichage du nombre de photos renvoyées -->
        <?= (!isset($alert['messageAlert'])) ? count($tabPhotos) .' ' . PHOTOSELECT .'<br/>' : '0 '. PHOTOSELECT . '<br/>' ?>
    </div>
</div>

<form action="" method="get">
    <label for="monselect"><?= MESSAGE_SAISIE ?></label>
    <select id="monselect" name="monselect">                <!-- liste déroulante -->
        <option value="tout" <?=isset($_GET['monselect']) && $_GET['monselect'] == "tout" ? 'selected' : '' ?>><?= ALL_PHOTOS ?></option> 
        <?php               // une option par categorie
            foreach ($tabAllCat as $cat)
            { 
                ?>
                <option value="<?=$cat->getCatId()?>" <?= (isset($_GET['monselect']) && $_GET['monselect'] == $cat->getCatId()) ? 'selected' : '' ?>>
                <?=$cat->getNomCat()?></option>     <!-- focus sur la catégorie qui correspond à celle selectionnée -->
                <?php 
            } 
        ?>
    </select>
    <input type="submit" value="<?=VALIDER?>"/>     <!-- envoi des resultats par la méthode get avec le parametre monselect-->
</form>

<!--  Début de la page -->
<h1><?= MENU_ACCUEIL ?></h1>

<div class = "col-md-10 col-sm-12 col-xs-12">
    <?php 
        if(!isset($alert['messageAlert']))
        {
            foreach($tabPhotos as $photo)           //affichage de toutes les photos grace a l'objet photo créé dans le controleur
            {
                ?>
                <a href="index.php?page=affichage&photo=<?=$photo->getPhotoId()?>" title="<?=INFODEST?>" > <img src='<?=PATH_IMAGES.'/'.$photo->getNomFich()?>' alt='<?=$photo->getDescription()?>' class="img-thumbnail"/></a>
                <?php                           // creation d'un lien qui renvoie vers c_affichage quand on clique sur une photo 
            }                                   // et passage en parametre du numero de la photo à afficher 
        }                                       // et affichage d'une description de la page d'arrivée
    ?>                                          
</div>
<!--  Fin de la page -->

<!--  Pied de page -->
<?php require_once(PATH_VIEWS.'footer.php');