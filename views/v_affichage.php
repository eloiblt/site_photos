<?php
/*
 * DS PHP
 * Vue page index - page d'accueil
 *
 * Copyright 2016, Eric Dufour
 * http://techfacile.fr
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 *
 */
//  En tête de page
?>
<?php require_once(PATH_VIEWS.'header.php');?>

<!--  Zone message d'alerte -->
<?php require_once(PATH_VIEWS.'alert.php');?>

<!--  Début de la page -->
<h1><?= TITRE_AFFICHAGE ?></h1>

<?php
if (!isset($alert['messageAlert']))
{
    ?>
    <div class = "col-md-6 col-sm-6 col-xs-12">
    <img src='<?=PATH_IMAGES . '/' . $photo -> getNomFich()?>'' alt='<?=  $photo -> getDescription()?>'/>    <!--affichage de la photo -->
    </div>
    <div class = "col-md-6 col-sm-6 col-xs-12">
        <table class="table table-bordered">
            <tr>
                    <tr><th><?=DESCRIPTION ?></th><td><?= $photo -> getDescription()?></td></tr>        <!--affichage de la description-->
                    <tr><th><?=NOMFICH?></th><td><?= $photo -> getNomFich()?></td></tr>         <!--affichage du nom-->
                    <tr><th><?=CATEGORIE?></th><td><a href="?monselect=<?=$photo->getCatId()?>"><?=$categorieDao -> getCategorieById($photo -> getCatId())?></a></td></tr>
                      <!-- affichage du nom de la categorie -->
            </tr>
        </table>
    </div>
    <?php 
        echo '<p><a href="index.php?page=suppr&photo=' . $photo -> getPhotoId() . '">Supprimer cette photo !</a></p>';
}
?>  
<!--  Fin de la page -->

<!--  Pied de page -->
<?php require_once(PATH_VIEWS.'footer.php'); 
