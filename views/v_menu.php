<?php
/*
 * TP PHP
 * Vue menu
 *
 * Copyright 2016, Eric Dufour
 * http://techfacile.fr
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 *
 * menu: http://www.w3schools.com/bootstrap/bootstrap_ref_comp_navs.asp
 */
?>
<!-- Menu du site -->

<nav class="navbar navbar-default">
  	<div class="container-fluid">
		<ul class="nav navbar-nav">
			<li <?= ($page=='accueil') ? 'class="active"' : '' ?>>
				<a href="index.php">
					<?= MENU_ACCUEIL ?>
				</a>
			</li>
			<li <?= ($page=='addphoto') ? 'class="active"' : '' ?>>
				<?php
					if (isset($_SESSION['login']) && $_SESSION['login'] == ADMIN)
					{
						?>
						<a href="index.php?page=addphoto">Ajouter des photos</a>
						<?php			
					}
				?>
			</li>
			<li <?= ($page=='addcat') ? 'class="active"' : '' ?>>
				<?php
					if (isset($_SESSION['login']) && $_SESSION['login'] == ADMIN)
					{
						?>
						<a href="index.php?page=addcat">Ajouter des catégories</a>
						<?php			
					}
				?>
			</li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li <?= ($page=='connexion') ? 'class="active"' : '' ?>>
				<?php 
					if (isset($_SESSION['login']))
					{
						?>
						<a href="index.php?page=deconnexion">Deconnexion</a>
						<?php			
					}
					else
					{
						?>
							<a href="index.php?page=connexion">Connexion</a>
						<?php
					}	
				?>
			</li>
		</ul>
  	</div>
</nav>


