<?php

function choixAlert($message)
{
	$alert = array();
	switch($message)
	{
		case 'query' :
			$alert['messageAlert'] = ERREUR_QUERY;
			break;
		case 'url_non_valide' :
			$alert['messageAlert'] = TEXTE_PAGE_404;
			break;
		case 'Pas_Photo':
			$alert['messageAlert'] = ERREUR_PHOTO;
			break;
		case 'Photo_Inexistante':
			$alert['messageAlert'] = ERREUR_PHOTO_INEXISTANTE;
			break;
		case 'User_Inconnu':
			$alert['messageAlert'] = ERR_CO;
			break;
		case 'Pseudo_pris':
			$alert['messageAlert'] = PSEUDO_PRIS;
			break;
		case 'Infos_vides':
			$alert['messageAlert'] = INFO_NULLES;
			break;
		case 'Cat_existante':
			$alert['messageAlert'] = CAT_EXISTANTE;
			break;
		default :
			$alert['messageAlert'] = MESSAGE_ERREUR;
	}
    return $alert;
}
